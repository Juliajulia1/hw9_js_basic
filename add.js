// Практичні завдання
// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//     Використайте 2 способи для пошуку елементів.
//     Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
// 2. Змініть текст усіх елементів h2 на "Awesome feature".
// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".оклику

// 1.
// Перший спосіб
// const element = document.getElementsByClassName('feature')
// console.log(element)

// Другий спосіб
const elements = document.querySelectorAll('.feature')
console.log(elements)


elements.forEach(function(elem) {
    elem.style.textAlign= 'center';
    elem.style.backgroundColor= 'yellow';
});

// 2.
// -----Перший спосіб
// const textTitle = document.querySelectorAll('h2')
// console.log(textTitle)
// textTitle.forEach(title =>{
//     title.textContent = 'Awesome feature'
// })

//
// // -----Другий спосіб
const titleText = document.getElementsByTagName('h2')
console.log(titleText)

function renameTitle(titles) {
    for (let i = 0; i<titleText.length; i++){
        titles[i].textContent = 'Awesome feature';
    }
}
// renameTitle(titleText)

// 3.
let features = document.getElementsByClassName("feature-title");

function addSymbol (symbol) {
    for (let i = 0; i<features.length; i++){
        symbol[i].append('!')
    }
}
addSymbol(features)



